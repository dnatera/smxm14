#no funciona porque es una palabra reservada
#else = 2

#no funciona porque es una palabra reservada
#false = 3

#Palabra reservada
#class = "Teorema avanzado de Zymurgy"

#Poniendo esto nos muestra todas las palabras reservadas
#help('keywords')

#Las variables son int
x = 1
y = 2
print(x + y)

#Cuando quiero declarar una cadena, le tengo que poner comillas
#Las variables son str
x = "1"
y = "2"
print(x + y)

#Atividad
#Indica porque son valides o no les següents variables

#Porque no funciona este codigo
#A = 2
#print(a)
#No funciona porque el nombre de la variable no es el mismo.

#Porque no funciona esta linea de codigo?
#print (a)
#a = 45
#No funciona porque el valor de la variable esta puesta despues del print, y el programa lo que hace es ejecutar los comandos consecutivamente

# Ejemplo float(el float sirve para poner decimales)
#Transformamos a float
#a = 10
#print (float(a))

#No transformamos, ya es float
#b = 5.0
#print(b)

print(5>3)